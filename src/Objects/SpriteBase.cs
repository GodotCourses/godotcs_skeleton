using System;
using Godot;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Class for base sprite
  /// </summary>
  public class SpriteBase : Area2D {
    /// <summary>
    /// Minimal velocity
    /// </summary>
    [Export] public readonly Vector2 MinVelocity = new Vector2();

    /// <summary>
    /// Maxnimal velocity
    /// </summary>
    [Export] public readonly Vector2 MaxVelocity = new Vector2();

    /// <summary>
    /// Rotation Speed
    /// </summary>
    [Export] public readonly float RotationSpeed = 0;

    /// <summary>
    /// True to Size the objet to the frame size of the current animation
    /// </summary>
    [Export] public readonly bool Autosize = true;

    /// <summary>
    /// Width
    /// </summary>
    [Export] public float Width;

    /// <summary>
    /// Height
    /// </summary>
    [Export] public float Height;

    /// <summary>
    /// Bounce on Top edge. If true, the sprite will bounce, if false, it will be destroyed.
    /// </summary>
    [Export] public bool BounceOnTop = false;

    /// <summary>
    /// Bounce on Right edge. If true, the sprite will bounce, if false, it will be destroyed.
    /// </summary>
    [Export] public bool BounceOnRight = false;

    /// <summary>
    /// Bounce on Bottom edge. If true, the sprite will bounce, if false, it will be destroyed.
    /// </summary>
    [Export] public bool BounceOnBottom = false;

    /// <summary>
    /// Bounce on Left edge. If true, the sprite will bounce, if false, it will be destroyed.
    /// </summary>
    [Export] public bool BounceOnLeft = false;

    /// <summary>
    /// Life
    /// </summary>
    [Export]
    public int Life {
      get => life;
      set {
        life = value;
        if (life <= 0) Destroy();
      }
    }

    private int life;

    /// <summary>
    /// Bounce on Left edge. If true, the sprite will bounce, if false, it will be destroyed.
    /// </summary>
    [Export] public SpriteStatus Status = SpriteStatus.Normal;

    /// <summary>
    /// Damages
    /// </summary>
    [Export] public int Damages = 0;

    /// <summary>
    /// Sprite Type Id
    /// </summary>
    public int TypeId;

    /// <summary>
    /// Velocity
    /// </summary>
    public Vector2 Velocity;

    /// <summary>
    /// Random generator object
    /// </summary>
    protected Random Rnd;

    // Various variables
    private const AnimationType CurrentAnimation = AnimationType.Idle;
    private const int ImpactEffect = 10;

    // References to components of the current object
    // *********************
    /// <summary>
    /// Collision Node
    /// </summary>
    protected CollisionShape2D CollisionNode;

    /// <summary>
    /// Animation Node
    /// </summary>
    protected AnimatedSprite AnimationNode;

    // References to the global scenes
    // *********************
    /// <summary>
    /// The root (main) node of the scene
    /// </summary>
    protected Node RootNode;

    /// <summary>
    /// The game node of the scene
    /// </summary>
    protected Game GameNode;

    /// <inheritdoc />
    public override void _Ready () {
      /* NMP:    GameNode = (Game) Utils.GetRootNode(this); */
      Viewport root = GetTree().GetRoot();
      RootNode = root.GetChild(root.GetChildCount() - 1);
      GameNode = (Game)RootNode;
      if (GameNode == null) Utils.Log("Game Node is missing", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      CollisionNode = (CollisionShape2D)GetNode("Collision");
      if (CollisionNode == null) Utils.Log("Collision Node is missing", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      AnimationNode = GetNode("Animation") as AnimatedSprite;
      if (AnimationNode == null) Utils.Log("Animation Node is missing", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      // connect collision signal
      if (CollisionNode != null)
        Connect("area_entered", this, "OnAreaEntered");

      Rnd = new Random();
      // adapt the size of the collision shape to the sprite texture
      if (Autosize && AnimationNode != null && CollisionNode != null) {
        Width = AnimationNode.Frames.GetFrame(CurrentAnimation.ToString(), 0).GetWidth();
        Height = AnimationNode.Frames.GetFrame(CurrentAnimation.ToString(), 0).GetHeight();
        // NOTE : does not us a classical cast for RectangleShape2D because it can cause a cast error and break the code
        if (CollisionNode.Shape is RectangleShape2D rectShape) {
          Vector2 rectShapeExtents = new Vector2(Width / 2f, Height / 2f);
          rectShape.Extents = rectShapeExtents;
          //Utils.Trace("Autosizing of " + Name + ": width=" + Width + " height=" + Height);
        }

        AnimationPlay(CurrentAnimation);
      }

      // get a random velocity
      Velocity = new Vector2(Rnd.Next((int)MinVelocity.x, (int)MaxVelocity.x), Rnd.Next((int)MinVelocity.y, (int)MaxVelocity.y));
    }

    /// <inheritdoc />
    public override void _Process (float delta) {
      Translate(Velocity * delta);
      if (Mathf.Abs(RotationSpeed) > 0) Rotate(RotationSpeed * delta);
      var minX = -Width / 2f;
      var maxX = GetViewportRect().Size.x - Width;
      var minY = -Height / 2f;
      var maxY = GetViewportRect().Size.y - Height;

      var isInArea = true;

      // Enemy bounces or is destroyed if it overtakes the top edge
      if (Position.y <= minY) {
        isInArea = false;
        if (BounceOnTop)
          Velocity.y = Mathf.Abs(Velocity.y);
        else if (Status != SpriteStatus.Sleeping) Destroy();
      }

      // Enemy bounces or is destroyed if it overtakes the right edge
      if (Position.x > maxX) {
        isInArea = false;
        if (BounceOnLeft)
          Velocity.x = -Mathf.Abs(Velocity.x);
        else if (Status != SpriteStatus.Sleeping) Destroy();
      }

      // Enemy bounces or is destroyed if it overtakes the bottom edge
      if (Position.y > maxY) {
        isInArea = false;
        if (BounceOnBottom)
          Velocity.y = -Mathf.Abs(Velocity.y);
        else if (Status != SpriteStatus.Sleeping) Destroy();
      }

      // Enemy bounces or is destroyed if it overtakes the left edge
      if (Position.x <= minX) {
        isInArea = false;
        if (BounceOnLeft)
          Velocity.x = Mathf.Abs(Velocity.x);
        else if (Status != SpriteStatus.Sleeping) Destroy();
      }

      // shall we awake the sprite when is whitin the area
      if (isInArea && Status == SpriteStatus.Sleeping) Status = SpriteStatus.Normal;
    }

    /// <summary>
    /// Destroy the object
    /// </summary>
    public virtual void Destroy (bool addScore = false) {
      //Utils.Trace(Name + " has been destroyed");
      Status = SpriteStatus.Destroyed;
      QueueFree();
    }

    /// <summary>
    /// Play an animation
    /// </summary>
    /// <param name="type">Animation Type</param>
    /// <param name="reverse">True to play the reverse form</param>
    public void AnimationPlay (AnimationType type, bool reverse = false) {
      if (AnimationNode == null || CurrentAnimation == type) return;
      AnimationNode.Play(type + (reverse ? "_reverse" : ""));
    }

    /// <summary>
    /// Collision trigger function
    /// </summary>
    /// <param name="source"></param>
    // ReSharper disable once UnusedMember.Global
    public void OnAreaEntered (Node source) {
      // enemy takes damages ?
      if (IsInGroup("player") && source.IsInGroup("enemy")) {
        //Utils.Trace(Name + " (player's projectile) touchs enemy " + source.Name);
        if (source is Enemy anEnemy) {
          if (anEnemy.Status != SpriteStatus.Destroyed && anEnemy.Status != SpriteStatus.Sleeping) {
            // add an impact effect
            anEnemy.Translate(new Vector2(ImpactEffect, 0));
            // add damages
            if (anEnemy.Status != SpriteStatus.Invincible) anEnemy.Life -= Damages;
            // camera shake
            GameNode.Camera.Shake(new Vector2(10, 10), 0.15f);
            // destroy projectile with flash
/*
            if (this is Projectile aProjectile) {
              aProjectile.CreateFlash();
              Destroy();
            }
            */
          }
        }
      }

      // player takes damages ?
      else if (IsInGroup("enemy") && source.IsInGroup("player")) {
        //Utils.Trace(Name + " (enemy's projectile) touchs player " + source.Name);
        if (source is Player aPlayer) {
          if (aPlayer.Status != SpriteStatus.Destroyed && aPlayer.Status != SpriteStatus.Sleeping) {
            // add an impact effect
            aPlayer.Translate(new Vector2(-ImpactEffect, 0));
            // camera shake
            GameNode.Camera.Shake(new Vector2(10, 10), 0.15f);
            // add damages
            if (aPlayer.Status != SpriteStatus.Invincible) aPlayer.Life -= Damages;
            //Utils.Trace(Name + " Life = " + Life);
            // destroy projectile with flash
            /*
            if (this is Projectile aProjectile) {
              aProjectile.CreateFlash();
              Destroy();
            }
            */
          }
        }
      }
    }

    /// <summary>
    ///  Dynamicaly create a explosion FX
    /// </summary>
    public Node2D CreateExplosion () {
      return Utils.InstanceScene("res://Scenes/Explosion.tscn", this);
    }

    /// <summary>
    ///  Dynamicaly create a explosion Flash FX
    /// </summary>
    public Node2D CreateExplosionFlash () {
      return Utils.InstanceScene("res://Scenes/ExplosionFlash.tscn", this);
    }
  }
}
