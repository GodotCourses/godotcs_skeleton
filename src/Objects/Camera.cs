using System;
using Godot;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Class for Camera
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public class Camera : Camera2D {
    /// <summary>
    /// Shake Movement amplitude
    /// </summary>
    [Export] public Vector2 ShakeAmplitude;

    /// <summary>
    /// Shake Movement duration
    /// </summary>
    [Export] public float ShakeDuration;

    private Random rnd;
    private float shakeTimer;
    private Vector2 shakeStoredPosition;

    /// <inheritdoc />
    public override void _Ready () {
      rnd = new Random();
    }

    /// <inheritdoc />
    public override void _Process (float delta) {
      base._Process(delta);

      // shall we shake ?
      if (shakeTimer > 0) {
        var shakeOffset = new Vector2(rnd.Next((int)-ShakeAmplitude.x, (int)ShakeAmplitude.x), rnd.Next((int)-ShakeAmplitude.y, (int)ShakeAmplitude.y));
        Position = shakeStoredPosition + shakeOffset;
        shakeTimer -= delta;
        //Utils.Trace("--------Shaking:" + shakeOffset);
      }
      // check if shake Has Been Started at least once
      // ReSharper disable once CompareOfFloatsByEqualityOperator
      else if (shakeTimer != 0) {
        Position = shakeStoredPosition;
        //Utils.Trace("End Shaking");
      }
    }

    /// <summary>
    /// Shake the camera
    /// </summary>
    /// <param name="amplitude"></param>
    /// <param name="duration"></param>
    public void Shake (Vector2 amplitude, float duration) {
      // if shake is running we can only raise the amplitude or duration
      if (shakeTimer > 0 && amplitude < ShakeAmplitude) return;
      if (amplitude.x > 0 && amplitude.y > 0) ShakeAmplitude = amplitude;

      // if shake is running we can only raise the amplitude or duration
      if (shakeTimer > 0 && duration < ShakeDuration) return;
      if (duration > 0) ShakeDuration = duration;

      // start shaking
      shakeTimer = ShakeDuration;
      shakeStoredPosition = Position;
      //Utils.Trace("-------Start Shaking for " + ShakeDuration + " at " + ShakeAmplitude);
    }
  }
}
