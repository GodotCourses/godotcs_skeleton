using System;
using Godot;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Class for an player
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public class Player : SpriteBase {
    /// <summary>
    /// Movement Speed
    /// </summary>
    [Export] public int Speed = 200;

    /// <summary>
    /// Score of the player
    /// </summary>
    public int Score;

    /// <summary>
    /// Level of the player
    /// </summary>
    public int Level;

    // Various variables
    // *********************
    private AnimationType oldAnimation;

    // References to components of the current object
    // *********************
    //private Node2D weaponSlotsNode;

    /// <inheritdoc />
    public override void _Ready () {
      base._Ready();
      AddToGroup("player");
      PlayerReset();
    }

    /// <inheritdoc />
    public override void _Process (float delta) {
      Vector2 translation = new Vector2();
      var deltaSpeed = Speed * delta;

      // movements of the player
      // ***************
      if (Input.IsActionPressed("ui_right")) {
        oldAnimation = AnimationType.Right;
        //Utils.Log("Move " + oldAnimation, Utils.Gcm);
        AnimationPlay(oldAnimation);
        translation.x += 1;
      } else if (Input.IsActionPressed("ui_left")) {
        oldAnimation = AnimationType.Left;
        //Utils.Log("Move " + oldAnimation, Utils.Gcm);
        AnimationPlay(oldAnimation);
        translation.x -= 1;
      }

      if (Input.IsActionPressed("ui_up")) {
        oldAnimation = AnimationType.Up;
        //Utils.Log("Move " + oldAnimation, Utils.Gcm);
        AnimationPlay(oldAnimation);
        translation.y -= 1;
      } else if (Input.IsActionPressed("ui_down")) {
        oldAnimation = AnimationType.Down;
        //Utils.Log("Move " + oldAnimation, Utils.Gcm);
        AnimationPlay(oldAnimation);
        translation.y += 1;
      }

      // Main attack
      // ***************
      if (Input.IsActionJustPressed("action_1")) {
        //TODO : Main attack
        Utils.Trace("Player Main attack");
      }

      if (Math.Abs(translation.Length()) > .1) {
        Position += translation.Normalized() * deltaSpeed;
      } else {
        if (oldAnimation == AnimationType.Idle)
          AnimationPlay(AnimationType.Idle);
        else
          AnimationPlay(oldAnimation, true);
      }

      // set the player size to the size of the current frame
      if (AnimationNode != null) {
        Width = AnimationNode.Frames.GetFrame(oldAnimation.ToString(), 0).GetWidth();
        Height = AnimationNode.Frames.GetFrame(oldAnimation.ToString(), 0).GetHeight();
      }

      // prevent player from going out of the screen
      var minX = Width / 2f;
      var maxX = GetViewportRect().Size.x - minX;
      var minY = Height / 2f;
      var maxY = GetViewportRect().Size.y - minY;
      Position = new Vector2(Mathf.Clamp(Position.x, minX, maxX), Mathf.Clamp(Position.y, minY, maxY));
    }

    /// <inheritdoc />
    /// <summary>
    /// Destroy the object
    /// </summary>
    public override void Destroy (bool addScore = false) {
      base.Destroy(addScore);

      GameNode.End();
    }

    /// <summary>
    /// Reset the player stats
    /// </summary>
    private void PlayerReset () {
      Level = 1;
      Score = 0;
    }
  }
}
