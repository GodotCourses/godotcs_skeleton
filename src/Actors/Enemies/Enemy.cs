using System;
using Godot;
using Godot.Collections;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Class for an Enemy
  /// </summary>
// ReSharper disable once ClassNeverInstantiated.Global
  public class Enemy : SpriteBase {
    /// <summary>
    /// List of enemy scene
    /// </summary>
    public static readonly Dictionary<EnemyType, PackedScene> EnemyScenes = new Dictionary<EnemyType, PackedScene>();

    /// <summary>
    /// Populate the enemy scene list
    /// </summary>
    public static void PreloadScenes () {
      // load all the resource associated to each type
      foreach (EnemyType type in Enum.GetValues(typeof(EnemyType))) {
        var path = "res://Scenes/" + type + ".tscn";

        if (ResourceLoader.Load(path) is PackedScene enemy) {
          Utils.Log("Add a " + type + " named " + enemy.GetName() + " to the list", Utils.GetCurrentMethodName());
          EnemyScenes.Add(type, enemy);
        } else {
          Utils.Log("Scene " + path + " not found for EnemyType " + type, Utils.GetCurrentMethodName(), Utils.LevelError);
        }
      }
    }
  }
}
