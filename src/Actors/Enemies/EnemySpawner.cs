using System;
using Godot;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Class for an Enemy spawner
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public class EnemySpawner : Node2D {
    /// <summary>
    /// Minimal Delay before next spawn in millisecond
    /// </summary>
    [Export] private int minimalDelayMs = 500;

    /// <summary>
    /// Maximal Delay before next spawn in millisecond
    /// </summary>
    [Export] private int maximalDelayMs = 1500;

    /// <summary>
    /// Maximal Number of enemy on the screen
    /// </summary>
    [Export] private int enemyLimit; // set to 0 (default init value) to let the game choose the value

    // Various variables
    private float elapsedTime;
    private float currentDelay;
    protected Random Rnd;
    public EnemySpawner () {
      enemyLimit = 0;
    }

    /// <inheritdoc />
    public override void _Ready () {
      Rnd = new Random();

      currentDelay = Rnd.Next(minimalDelayMs, maximalDelayMs) / 1000f;

      if (enemyLimit == 0) enemyLimit = ChooseEnemyLimit();
    }

    /// <inheritdoc />
    public override void _Process (float delta) {
      elapsedTime += delta;
      if (elapsedTime > currentDelay) {
        currentDelay = Rnd.Next(minimalDelayMs, maximalDelayMs) / 1000f;
        elapsedTime = 0;
        Spawn();
      }
    }

    /// <summary>
    /// Fix the simultaneous displayed enemies limit
    /// </summary>
    /// <returns>Index in the enemy type list</returns>
    private static int ChooseEnemyLimit () {
      // TODO: change the enemy limit using the player's level
      const int limit = 8;
      //Utils.Trace("enemy limit=" + limit);
      return limit;
    }

    /// <summary>
    /// Choose an enemy type
    /// </summary>
    /// <returns>Index in the enemy type list</returns>
    private int SelectEnemyIndex () {
      // TODO: improve the enemy selection using the player's level
      var randIndex = Rnd.Next(0, Enemy.EnemyScenes.Count);
      // randIndex = Rnd.Next(4, 5); // todo : test only
      return randIndex;
    }

    /// <summary>
    /// Spam an enemy
    /// </summary>
    private void Spawn () {
      if (Enemy.EnemyScenes.Count > 0 && GetChildCount() < enemyLimit) {
        // choose an enemy type
        var randIndex = SelectEnemyIndex();
        //tips:get an enum value byt its position in the enum
        EnemyType randEnemyType = (EnemyType)randIndex;
        PackedScene enemyScene = Enemy.EnemyScenes[randEnemyType];

        if (enemyScene != null) {
          SpriteBase enemyInstance = (SpriteBase)enemyScene.Instance();
          // choose enemy position
          var posX = GetViewportRect().Size.x + enemyInstance.Width;
          var minY = (int)enemyInstance.Height;
          var maxY = (int)GetViewportRect().Size.y - minY;
          var posY = Rnd.Next(minY, maxY);
          enemyInstance.Position = new Vector2(posX, posY);
          enemyInstance.TypeId = randIndex;
          // add it to the current scene
          AddChild(enemyInstance);

          // 50% change is vertitcal velocity direction
          if (Rnd.Next(0, 2) == 0) {
            //Utils.Trace(Name + " Changes Y direction");
            enemyInstance.Velocity.y = -enemyInstance.Velocity.y;
          }

          enemyInstance.AnimationPlay(AnimationType.Idle);
        } else {
          Utils.Log("An EnemyScene is not found in EnemyScenes", Utils.GetCurrentMethodName(), Utils.LevelError);
        }
      }
    }
  }
}
