using Godot;

namespace GodotCS_Skeleton.Scripts {
  /// <inheritdoc />
  /// <summary>
  /// Main game class
  /// </summary>
  // ReSharper disable once ClassNeverInstantiated.Global
  public class Game : Node {
    // References to the global objects
    // *********************
    /// <summary>
    /// Player
    /// </summary>
    public Player Player;

    /// <summary>
    /// Camera
    /// </summary>
    public Camera Camera;

    /// <summary>
    /// Enemy spawner
    /// </summary>
    public EnemySpawner EnemySpawner;

    /// <inheritdoc />
    public override void _Ready () {
      Player = (Player)GetNode("Player");
      if (Player == null)
        Utils.Log("Player has not been found", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      Camera = (Camera)GetNode("Camera");
      if (Camera == null)
        Utils.Log("Camera has not been found", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      EnemySpawner = (EnemySpawner)GetNode("EnemySpawner");
      if (EnemySpawner == null)
        Utils.Log("EnemySpawner has not been found", Utils.GetCurrentMethodName(), Utils.LevelFatal);

      // fill the list with scenes types
      Enemy.PreloadScenes();

      // change some scenes properties
      Camera.AnchorMode = Camera2D.AnchorModeEnum.FixedTopLeft;
      Camera.Current = true;
    }

    /// <inheritdoc />
    public override void _Process (float delta) {
      if (Input.IsActionPressed("ui_cancel")) QuitGame();
    }

    /// <summary>
    /// Quit the game
    /// </summary>
    public void QuitGame () {
      //TODO: add some stuff to confirm before quit
      Utils.Log("Quiting game...");
      GetTree().Quit();
    }

    /// <summary>
    /// Quit the game
    /// </summary>
    /// <param name="hasWin">True if player has win</param>
    public void End (bool hasWin = false) {
      // TODO: add some stuff for win and lose game
      if (hasWin) {
        Utils.Log("player has win...");
      } else {
        Utils.Log("player has lose...");
      }

      QuitGame();
    }
  }
}
