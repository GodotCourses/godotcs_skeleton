﻿// ReSharper disable MissingXmlDoc

/// <summary>
/// Type of Animations
/// </summary>
public enum AnimationType {
  // must match the animation names of the sprite
  Up,
  Right,
  Down,
  Left,
  Idle,
  FadeOut
}
