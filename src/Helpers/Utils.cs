using System;
using System.Diagnostics;
using Godot;
using Object = Godot.Object;

internal static class Utils {
	/// <summary>
	/// Warning Levels for log message
	/// </summary>
	public const int LevelWarning = 1;

	public const int LevelError = 2;

	public const int LevelFatal = 99;

	/// <summary>
	/// compute the full method name (where the call is made)
	/// </summary>
	/// <returns></returns>
	public static string GetCurrentMethodName () {
		StackTrace stackTrace = new StackTrace();
		StackFrame stackFrame = stackTrace.GetFrame(1);
		return stackFrame.GetMethod().DeclaringType +
			" :: " + stackFrame.GetMethod().Name;
	}

	/// <summary>
	/// Add an entry to the project LOG
	/// </summary>
	/// <param name="message">Message to log</param>
	/// <param name="context">Context (for instance the objet:method) </param>
	/// <param name="level">Level og log</param>
	public static void Log (string message = "Enter", string context = "",
																									int level = LevelWarning) {
		var entry = DateTime.Now + " ## " + level;
		if (context != "")
			entry += " ## " + context;
		entry += ": " + message;
		// Show the full message in the console output
		GD.Print(entry);

		if (level == LevelFatal) {
			Node node = new Node();
			node.GetTree().Quit();
		}
	}

	/// <summary>
	/// Add a simple trace in the console output
	/// </summary>
	/// <param name="message"></param>
	public static void Trace (string message = "") {
		if (message == "")
			message = DateTime.Now.ToShortTimeString();
		var entry = "### TRACE " + message + " ###";
		// Show the full message in the console output
		GD.Print(entry);
	}

	/// <summary>
	/// generate a random float
	/// </summary>
	/// <param name="rand">Random object</param>
	/// <returns></returns>
	public static float NextFloat (Random rand) {
		var mantissa = (rand.NextDouble() * 2.0) - 1.0;
		var exponent = Math.Pow(2.0, rand.Next(-126, 128));
		return (float) (mantissa * exponent);
	}

	/// <summary>
	/// return a random float value between 2 values (can be negative)
	/// </summary>
	/// <param name="min">minimal value (can be negative)</param>
	/// <param name="max">maximal value (can be negative)</param>
	/// <param name="rand">Random object</param>
	/// <returns></returns>
	public static float RandomFloat (float min, float max, Random rand) {
		var random = NextFloat(rand);
		var diff = max - min;
		var r = random * diff;
		return min + r;
	}

	/// <summary>
	/// return a random element of an array of objects
	/// </summary>
	/// <param name="choices">array of objects</param>
	/// <returns>an object</returns>
	public static Object Choose (Object[] choices) {
		Random rnd = new Random();
		var index = rnd.Next(0, choices.Length);
		return choices[index];
	}

	/// <summary>
	/// Get the root node of the Scene thats contains a given node
	/// </summary>
	/// <param name="aNode">A node</param>
	/// <returns>The root Node</returns>
	public static Node GetRootNode (Node aNode) {
		Viewport root = aNode.GetTree().GetRoot();
		return root.GetChild(root.GetChildCount() - 1);
	}

	/// <summary>
	///  Dynamicaly create an instance of a scene
	/// </summary>
	public static Node2D InstanceScene (string path, Node2D parent) {
		if (path == "" || parent == null)
			return null;

		if (ResourceLoader.Load(path) is PackedScene scene) {
			Node2D instance = (Node2D) scene.Instance();
			instance.Position = parent.Position;

			// add it to the root node (to preserve its global position)
			Node rootNode = GetRootNode(parent);
			rootNode.AddChild(instance);
			return instance;
		}

		return null;
	}
}
