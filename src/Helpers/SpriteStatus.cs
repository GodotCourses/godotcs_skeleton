﻿// ReSharper disable MissingXmlDoc

/// <summary>
/// Status of sprites
/// </summary>
public enum SpriteStatus {
  Sleeping,
  Normal,
  Destroyed,
  Invincible
}
