﻿// ReSharper disable MissingXmlDoc

/// <summary>
/// Type of enemies
/// </summary>
public enum EnemyType {
  // must match the scenes name
  AsteroidStrange,
  AsteroidGrey,
  AsteroidRed,
  MobFlea,
  MobLouse,
  MobMantis,
  MobScarab,
  BossСricket,
  BossLocust
}
