## Godot_CS_Skeleton

### Description

Framework C# pour Godot Engine 3 (et suivants), regroupant les fonctionnalités utiles à un jeu vidéo.

### Objectifs

Aucun.

### Mouvements et actions du joueur

Déplacer le joueur: touches ZQSD ou bien les flèches du clavier.
Quitter le jeu: clic sur la croix ou touche escape.

### Interactions

Aucun.

### Copyrights

Ecrit en C# en utilisant le moteur de jeu Godot Engine.
Développé en utilisant principalement Visual Studio Code et/ou JetBrain Rider.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)

=================================================

### Description

C# Base Framework for Godot Engine 3 (and more), gathering the useful features for a video game.

### Goals

None.

### Movements and actions of the player

Move the player: ZQSD keys or the arrows on the keyboard.
Exit the game: click on the cross or escape key.

### Interactions

None.

### Copyrights

Written in C# using the Godot Game Engine.
Developed mainly using Visual Studio Code and/or JetBrain Rider.

-----------------
(C) 2020 GameaMea (http://www.gameamea.com)
