# Naming conventions

## General naming conventions

Never include spaces in the filenames. Spaces can cause issues with command line tools, which we use to automate tasks.

We generally name files, variables, and classes starting with keywords that help group and distinguish similar elements.

For example, instead of:

  var walk_speed
  var run_speed
  var sprint_speed
  var max_speed

We favor:

  var speed_walk
  var speed_run
  var speed_sprint
  var speed_max

This helps to group and find related variables through autocompletion. In this case, if you type sp, all four speed-related variables will appear in the completion menu.

## Naming in the assets folder

Use lowercase for the folder names to distinguish them from the source code.

Name the assets using snake_case

  body.png
  arm_right.png
  sword_hit.ogg
  theme.tres